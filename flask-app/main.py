# from flask import Flask

# app = Flask(__name__)

# @app.route('/home')
# def home():
#     return "Flask App in MyBinder"

# app.run(port=5030)

# import eventlet
# eventlet.monkey_patch()

import os

os.environ['DATABASE_URL'] = 'sqlite:///:memory:'

from hemlock import Branch, Page, Label, route

@route('/survey')
def start():
    return Branch(
        Page(
            Label("Hello, World")
        ),
        Page(
            Label("Goodbye, World!"),
            terminal=True
        )
    )

from hemlock import create_app

app = create_app()
app.run(port=5030)

# if __name__ == "__main__":
#     from hemlock.app import socketio
#     socketio.run(app, port=5030)