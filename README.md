## Flask app on Binder test

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/dsbowen%2Fflask-binder-test/HEAD)

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/dsbowen%2Fflask-binder-test/HEAD?urlpath=/proxy/5030/home)


This repository tests a Flask app running in binder.